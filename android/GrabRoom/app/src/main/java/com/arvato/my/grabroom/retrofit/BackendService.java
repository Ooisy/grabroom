package com.arvato.my.grabroom.retrofit;

import com.arvato.my.grabroom.data.model.Meeting;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by OOIS001 on 20/1/2018.
 */

public interface BackendService {

    @POST("book")
    Call<Boolean> book();

    @POST("book2")
    Call<Boolean> book2(@Body Meeting meeting);

}
