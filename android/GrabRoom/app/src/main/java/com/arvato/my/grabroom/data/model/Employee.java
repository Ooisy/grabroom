package com.arvato.my.grabroom.data.model;

import java.util.ArrayList;
import java.util.List;

public class Employee {

    private String name;
    private String email;
    private boolean isSelected;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public static List<Employee> getList(){
        List<Employee> employees = new ArrayList<>();

        Employee employee1 = new Employee();
        employee1.setName("Sanket");
        employee1.setEmail("sanket@bertelsmann.de");
        employees.add(employee1);

        Employee employee2 = new Employee();
        employee2.setName("Saranjeet");
        employee2.setEmail("saranjeet@bertelsmann.de");
        employees.add(employee2);

        Employee employee3 = new Employee();
        employee3.setName("Seng Hock");
        employee3.setEmail("senghock@bertelsmann.de");
        employees.add(employee3);

        Employee employee4 = new Employee();
        employee4.setName("Soon Ming");
        employee4.setEmail("soonming@bertelsmann.de");
        employees.add(employee4);

        Employee employee5 = new Employee();
        employee5.setName("Soon Yoong");
        employee5.setEmail("soonyoong@bertelsmann.de");
        employees.add(employee5);

        Employee employee6 = new Employee();
        employee6.setName("Soon Desmond");
        employee6.setEmail("soondesmond@bertelsmann.de");
        employees.add(employee6);

        return employees;

    }

    public static List<Employee> getListSoon(){
        List<Employee> employees = new ArrayList<>();

//        Employee employee1 = new Employee();
//        employee1.setName("Sanket");
//        employee1.setEmail("sanket@bertelsmann.de");
//        employees.add(employee1);
//
//        Employee employee2 = new Employee();
//        employee2.setName("Saranjeet");
//        employee2.setEmail("saranjeet@bertelsmann.de");
//        employees.add(employee2);
//
//        Employee employee3 = new Employee();
//        employee3.setName("Seng Hock");
//        employee3.setEmail("senghock@bertelsmann.de");
//        employees.add(employee3);

        Employee employee4 = new Employee();
        employee4.setName("Soon Ming");
        employee4.setEmail("soonming@bertelsmann.de");
        employees.add(employee4);

        Employee employee5 = new Employee();
        employee5.setName("Soon Yoong");
        employee5.setEmail("soonyoong@bertelsmann.de");
        employees.add(employee5);

        Employee employee6 = new Employee();
        employee6.setName("Soon Desmond");
        employee6.setEmail("soondesmond@bertelsmann.de");
        employees.add(employee6);

        return employees;

    }
}
