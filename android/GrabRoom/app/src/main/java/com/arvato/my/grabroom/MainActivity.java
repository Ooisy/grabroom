package com.arvato.my.grabroom;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.arvato.my.grabroom.data.model.Room;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AdapterView.OnItemSelectedListener {

    ProgressDialog dialog;
    TextView locationName;
    boolean  doubleClick = false;
    private boolean isBusy = false;
    private final Handler mHandler = new Handler();
    private int clicks;
    Dialog unAnvailableDialog;
    Button customOkBtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
////                        .setAction("Action", null).show();
//                startActivityForResult(new Intent(MainActivity.this, NewBookingActivity.class), 1);
//            }
//        });
//        fab.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View view) {
//                createNotification();
//                return false;
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //location drop downbox
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(this);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.location_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        locationName = (TextView)findViewById(R.id.location_name);

        createDialog();
        //Handle image click
        ImageView imgClick = (ImageView)findViewById(R.id.mapImage);
        imgClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isBusy) {
                    isBusy = true;
                    clicks++;

                    mHandler.postDelayed(new Runnable() {
                        public final void run() {

                            if (clicks >= 2) {  // Double tap
                                unAnvailableDialog.show();
                            }

                            if (clicks == 1) {  // Single tap
                                Intent intent = new Intent(MainActivity.this, InviteActivity.class);
                                Room room = new Room();
                                room.setName("Langkawi");
                                room.setId("RMB10013607");
                                room.setPax(12);
                                room.setPowerSocket(true);
                                room.setProjector(true);;
                                intent.putExtra("room", room);
                                startActivityForResult(intent, 1);
                            }
                            clicks = 0;
                        }
                    }, 200L);
                    isBusy = false;
                }

            }


        });

        imgClick.setOnLongClickListener(new View.OnLongClickListener(){

            @Override
            public boolean onLongClick(View view) {
               /* Intent intent = new Intent();
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this, 1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                Notification notification = new Notification.Builder(MainActivity.this)
                        .setContentTitle("GrabRoom")
                        .setContentText("Are you really using Penang room?")
                        .setVisibility(Notification.VISIBILITY_PUBLIC)
                        .setSmallIcon(R.drawable.baseline_meeting_room_black)
                        .addAction(R.drawable.ic_menu_manage, "Yes", pendingIntent)     // #2
                        .addAction(R.drawable.ic_menu_manage, "No", pendingIntent)     // #2
                        .build();

                NotificationManager notificationManager =
                        (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

                notificationManager.notify(1, notification);*/
                createNotification();
                return true;
            }
        });
        dialog = ProgressDialog.show(MainActivity.this, "",
                "Starting check your location...", true);
        new MainActivity.onSearchLocation().execute("");


    }

    private class onSearchLocation extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                Thread.interrupted();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            finishLoading();
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }

    private void finishLoading() {
        this.dialog.dismiss();
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Location");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                findViewById(R.id.map_main).setVisibility(View.VISIBLE);
            }
        });
        builder.setMessage("We detected you are at Malaysia, Gtower-Level 26");
        AlertDialog alert1 = builder.create();
        alert1.show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch(id){
            case R.id.nav_main:
                break;
            case R.id.nav_scan_qr:
                startActivity(new Intent(MainActivity.this, QrActivity.class));
                break;
            case R.id.nav_new_booking:
                startActivityForResult(new Intent(MainActivity.this, NewBookingActivity.class), 1);
                break;
            case R.id.nav_logout:
                startActivity(new Intent(MainActivity.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                break;
            case R.id.nav_sos:
                startActivity(new Intent(MainActivity.this, SosActivity.class));
                break;
            default:
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void createNotification(){
        // The id of the channel.
        String CHANNEL_ID = "my_channel_01";

//        NotificationCompat.Builder mBuilder =
//                new NotificationCompat.Builder(this, CHANNEL_ID)
//                        .setSmallIcon(R.drawable.baseline_meeting_room_black)
//                        .setContentTitle(getString(R.string.app_name))
//                        .setContentText("Are you really using Langkawi room");

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MainActivity.class);
        //resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your app to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        //stackBuilder.addNextIntent(new Intent(this, MainActivity.class));
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setSmallIcon(R.drawable.baseline_meeting_room_black)
                        .setContentTitle(getString(R.string.app_name))
                        .addAction(0, "Yes", resultPendingIntent)
                        .addAction(0, "No", resultPendingIntent)
                        .setAutoCancel(true)
                        .setContentText("Are you really using Langkawi room");

        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // mNotificationId is a unique integer your app uses to identify the
        // notification. For example, to cancel the notification, you can pass its ID
        // number to NotificationManager.cancel().
        mNotificationManager.notify(1, mBuilder.build());
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        switch(pos){
            case 0:
                locationName.setText("Malaysia, Gtower - Level 26");
                break;
            case 1:
                locationName.setText("Malaysia, Gtower - Level 27");
                break;
            case 2:
                locationName.setText("Latvia, Riga - Level 10");
                break;
            case 3:
                locationName.setText("German, Gutersloh");
                break;
            default:
                break;
        }
    }

    public void onNothingSelected(AdapterView<?> parent) {
    }

    private void createDialog()
    {
        unAnvailableDialog = new Dialog(this);

        //SET TITLE
        unAnvailableDialog.setTitle("Penang Meeting Room");

        //set content
        unAnvailableDialog.setContentView(R.layout.custom_layout);

        customOkBtn= (Button) unAnvailableDialog.findViewById(R.id.customOk);
        customOkBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                unAnvailableDialog.dismiss();
            }
        });

    }
}
