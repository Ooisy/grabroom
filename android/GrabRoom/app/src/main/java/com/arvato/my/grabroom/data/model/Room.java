package com.arvato.my.grabroom.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by oois001 on 6/9/2018.
 */

public class Room implements Parcelable {

    private String name;
    private String id;
    private int pax;
    private boolean projector;
    private boolean tv;
    private boolean teleconf;
    private boolean whiteboard;
    private boolean powerSocket;

    public Room(){}

    protected Room(Parcel in) {
        name = in.readString();
        id = in.readString();
        pax = in.readInt();
        projector = in.readByte() != 0;
        tv = in.readByte() != 0;
        teleconf = in.readByte() != 0;
        whiteboard = in.readByte() != 0;
        powerSocket = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(id);
        dest.writeInt(pax);
        dest.writeByte((byte) (projector ? 1 : 0));
        dest.writeByte((byte) (tv ? 1 : 0));
        dest.writeByte((byte) (teleconf ? 1 : 0));
        dest.writeByte((byte) (whiteboard ? 1 : 0));
        dest.writeByte((byte) (powerSocket ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Room> CREATOR = new Creator<Room>() {
        @Override
        public Room createFromParcel(Parcel in) {
            return new Room(in);
        }

        @Override
        public Room[] newArray(int size) {
            return new Room[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPax() {
        return pax;
    }

    public void setPax(int pax) {
        this.pax = pax;
    }

    public boolean isProjector() {
        return projector;
    }

    public void setProjector(boolean projector) {
        this.projector = projector;
    }

    public boolean isTv() {
        return tv;
    }

    public void setTv(boolean tv) {
        this.tv = tv;
    }

    public boolean isTeleconf() {
        return teleconf;
    }

    public void setTeleconf(boolean teleconf) {
        this.teleconf = teleconf;
    }

    public boolean isWhiteboard() {
        return whiteboard;
    }

    public void setWhiteboard(boolean whiteboard) {
        this.whiteboard = whiteboard;
    }

    public boolean isPowerSocket() {
        return powerSocket;
    }

    public void setPowerSocket(boolean powerSocket) {
        this.powerSocket = powerSocket;
    }

    public static List<Room> getList(){
        List<Room> rooms = new ArrayList<>();

        Room room1 = new Room();
        room1.setName("Kapalai");
        room1.setId("RMB10013613");
        room1.setPax(4);
        room1.setPowerSocket(true);
        rooms.add(room1);

        Room room2 = new Room();
        room2.setName("Lang Tengah");
        room2.setId("RMB10013610");
        room2.setPax(3);
        rooms.add(room2);

        Room room3 = new Room();
        room3.setName("Langkawi");
        room3.setId("RMB10013607");
        room3.setPax(12);
        room3.setPowerSocket(true);
        room3.setProjector(true);
        rooms.add(room3);

        Room room4 = new Room();
        room4.setName("Layang layang");
        room4.setPax(4);
        room4.setId("RMB10013605");
        rooms.add(room4);

        Room room5 = new Room();
        room5.setName("Okinawa");
        room5.setId("RMB10013611");
        room5.setPax(4);
        rooms.add(room5);

        Room room6 = new Room();
        room6.setName("Pangkor");
        room6.setId("RMB10013614");
        room6.setPax(8);
        room6.setTv(true);
        rooms.add(room6);

        Room room7 = new Room();
        room7.setName("Penang");
        room7.setId("RMB10013606");
        room7.setPax(12);
        room7.setProjector(true);
        rooms.add(room7);

        Room room8 = new Room();
        room8.setName("Perhentian");
        room8.setId("RMB10013608");
        room8.setPax(6);
        rooms.add(room8);

        Room room9 = new Room();
        room9.setName("Redang");
        room9.setId("RMB10013617");
        room9.setPax(8);
        rooms.add(room9);

        Room room10 = new Room();
        room10.setName("Sipadan");
        room10.setId("RMB10013612");
        room10.setPax(5);
        rooms.add(room10);

        Room room11 = new Room();
        room11.setName("Tioman");
        room11.setId("RMB10013618");
        room11.setPax(6);
        rooms.add(room11);

        return rooms;
    }

    public static List<Room> getListProjector(){
        List<Room> rooms = new ArrayList<>();

        Room room3 = new Room();
        room3.setName("Langkawi");
        room3.setId("RMB10013607");
        room3.setPax(12);
        room3.setPowerSocket(true);
        room3.setProjector(true);
        rooms.add(room3);

        Room room7 = new Room();
        room7.setName("Penang");
        room7.setId("RMB10013606");
        room7.setPax(12);
        room7.setProjector(true);
        rooms.add(room7);

        return rooms;
    }

    public static List<Room> getListTV(){
        List<Room> rooms = new ArrayList<>();

        Room room6 = new Room();
        room6.setName("Pangkor");
        room6.setId("RMB10013614");
        room6.setPax(8);
        room6.setTv(true);
        rooms.add(room6);

        Room room8 = new Room();
        room8.setName("Perhentian");
        room8.setId("RMB10013608");
        room8.setPax(6);
        rooms.add(room8);

        Room room9 = new Room();
        room9.setName("Redang");
        room9.setId("RMB10013617");
        room9.setPax(8);
        rooms.add(room9);

        Room room10 = new Room();
        room10.setName("Sipadan");
        room10.setId("RMB10013612");
        room10.setPax(5);
        rooms.add(room10);

        Room room11 = new Room();
        room11.setName("Tioman");
        room11.setId("RMB10013618");
        room11.setPax(6);
        rooms.add(room11);

        return rooms;
    }

    public static List<Room> getListWhiteboard(){
        List<Room> rooms = new ArrayList<>();

        Room room2 = new Room();
        room2.setName("Lang Tengah");
        room2.setId("RMB10013610");
        room2.setPax(3);
        rooms.add(room2);

        Room room3 = new Room();
        room3.setName("Langkawi");
        room3.setId("RMB10013607");
        room3.setPax(12);
        room3.setPowerSocket(true);
        room3.setProjector(true);
        rooms.add(room3);

        Room room7 = new Room();
        room7.setName("Penang");
        room7.setId("RMB10013606");
        room7.setPax(12);
        room7.setProjector(true);
        rooms.add(room7);

        return rooms;
    }

    public static List<Room> getListTeleCon(){
        List<Room> rooms = new ArrayList<>();

        Room room2 = new Room();
        room2.setName("Lang Tengah");
        room2.setId("RMB10013610");
        room2.setPax(3);
        rooms.add(room2);

        Room room3 = new Room();
        room3.setName("Langkawi");
        room3.setId("RMB10013607");
        room3.setPax(12);
        room3.setPowerSocket(true);
        room3.setProjector(true);
        rooms.add(room3);

        Room room7 = new Room();
        room7.setName("Penang");
        room7.setId("RMB10013606");
        room7.setPax(12);
        room7.setProjector(true);
        rooms.add(room7);

        Room room8 = new Room();
        room8.setName("Perhentian");
        room8.setId("RMB10013608");
        room8.setPax(6);
        rooms.add(room8);

        Room room10 = new Room();
        room10.setName("Sipadan");
        room10.setId("RMB10013612");
        room10.setPax(5);
        rooms.add(room10);

        Room room11 = new Room();
        room11.setName("Tioman");
        room11.setId("RMB10013618");
        room11.setPax(6);
        rooms.add(room11);

        return rooms;
    }

}
