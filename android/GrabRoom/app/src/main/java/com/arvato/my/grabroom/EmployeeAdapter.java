package com.arvato.my.grabroom;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;

import com.arvato.my.grabroom.data.model.Employee;

import java.util.List;

public class EmployeeAdapter extends ArrayAdapter<Employee> {

    private final Context mContext;

    private List<Employee> mList;
    private LayoutInflater mInflater;

    static class ViewHolder {
        protected CheckBox chkbxEmployee;

        public ViewHolder(View rootView) {
            chkbxEmployee = (CheckBox) rootView.findViewById(R.id.chkbxEmployee);
        }
    }

    public EmployeeAdapter(Context context, List<Employee> objects) {
        super(context, 0, objects);
        mContext = context;
        mList = objects;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Employee getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_employee, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.chkbxEmployee.setText(mList.get(position).getName());

        //viewHolder.chkbxEmployee.setTag(0, convertView);
        viewHolder.chkbxEmployee.setTag(position);
        viewHolder.chkbxEmployee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = (Integer)  viewHolder.chkbxEmployee.getTag();

                if(mList.get(position).isSelected()){
                    mList.get(position).setSelected(false);
                }else {
                    mList.get(position).setSelected(true);
                }
            }
        });

        return convertView;
    }

}
