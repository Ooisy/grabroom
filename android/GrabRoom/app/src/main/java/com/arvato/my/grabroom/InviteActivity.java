package com.arvato.my.grabroom;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TimePicker;

import com.arvato.my.grabroom.data.model.Employee;
import com.arvato.my.grabroom.data.model.Meeting;
import com.arvato.my.grabroom.data.model.Room;
import com.arvato.my.grabroom.retrofit.BackendService;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class InviteActivity extends AppCompatActivity {

    private AppCompatEditText edtxtSearch;

    private AppCompatEditText edtxtDateFrom;
    private AppCompatEditText edtxtDateTo;
    private AppCompatEditText edtxtSubject;
    private AppCompatEditText edtxtContent;

    private DatePickerDialog.OnDateSetListener onDateFromSetListener;
    private DatePickerDialog.OnDateSetListener onDateToSetListener;

    private TimePickerDialog.OnTimeSetListener onTimeFromSetListener;
    private TimePickerDialog.OnTimeSetListener onTimeToSetListener;

    private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());;

    private int yearFrom;
    private int monthFrom;
    private int dayFrom;

    private int yearTo;
    private int monthTo;
    private int dayTo;

    private String dateFrom;
    private String dateTo;

    private ListView lstvwEmployee;

    private Button btnSend;

    private EmployeeAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setTitle(((Room)getIntent().getParcelableExtra("room")).getName());

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
        edtxtContent = (AppCompatEditText) findViewById(R.id.edtxtContent);
        edtxtSubject = (AppCompatEditText) findViewById(R.id.edtxtSubject);

        if(getIntent().getStringExtra("dateTimeFrom")== null && getIntent().getStringExtra("dateTimeTo") == null){
            edtxtDateFrom = (AppCompatEditText) findViewById(R.id.edtxtDateFrom);
            edtxtDateTo = (AppCompatEditText) findViewById(R.id.edtxtDateTo);

            edtxtDateFrom.setFocusable(false);
            edtxtDateFrom.setClickable(true);

            edtxtDateTo.setFocusable(false);
            edtxtDateTo.setClickable(true);

            onDateFromSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    final Calendar calendar = Calendar.getInstance();
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minute = calendar.get(Calendar.MINUTE);

                    yearFrom = i;
                    monthFrom = i1;
                    dayFrom = i2;

                    TimePickerDialog timePickerDialog = new TimePickerDialog(InviteActivity.this, onTimeFromSetListener, hour, minute, true);
                    timePickerDialog.show();
                }
            };

            onTimeFromSetListener = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int i, int i1) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(yearFrom, monthFrom, dayFrom, i, i1);
                    edtxtDateFrom.setText(dateFormatter.format(newDate.getTime()));
                    dateFrom = dateFormatter.format(newDate.getTime());
                }
            };

            onDateToSetListener = new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                    final Calendar calendar = Calendar.getInstance();
                    int hour = calendar.get(Calendar.HOUR_OF_DAY);
                    int minute = calendar.get(Calendar.MINUTE);

                    yearTo = i;
                    monthTo = i1;
                    dayTo = i2;

                    TimePickerDialog timePickerDialog = new TimePickerDialog(InviteActivity.this, onTimeToSetListener, hour, minute, true);
                    timePickerDialog.show();
                }
            };

            onTimeToSetListener = new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int i, int i1) {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(yearTo, monthTo, dayTo, i, i1);
                    edtxtDateTo.setText(dateFormatter.format(newDate.getTime()));
                    dateTo = dateFormatter.format(newDate.getTime());
                }
            };

            edtxtDateFrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);

                    DatePickerDialog datePickerDialog = new DatePickerDialog(InviteActivity.this, onDateFromSetListener, year, month, day);
                    datePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
                    //datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                    datePickerDialog.show();
                }
            });

            edtxtDateTo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Calendar calendar = Calendar.getInstance();
                    int year = calendar.get(Calendar.YEAR);
                    int month = calendar.get(Calendar.MONTH);
                    int day = calendar.get(Calendar.DAY_OF_MONTH);

                    Calendar minCalendar = Calendar.getInstance();
                    if(yearFrom !=0 && monthFrom !=0 && dayFrom !=0){
                        minCalendar.set(yearFrom, monthFrom, dayFrom);
                    }

                    DatePickerDialog datePickerDialog = new DatePickerDialog(InviteActivity.this, onDateToSetListener, year, month, day);
                    datePickerDialog.getDatePicker().setMinDate(minCalendar.getTimeInMillis());
                    datePickerDialog.getDatePicker().setMaxDate(minCalendar.getTimeInMillis());
                    datePickerDialog.show();
                }
            });

            edtxtDateFrom.setVisibility(View.VISIBLE);
            edtxtDateTo.setVisibility(View.VISIBLE);
        } else {
            //getIntent().getStringExtra("dateTimeFrom")== null && getIntent().getStringExtra("dateTimeTo") == null
                dateFrom = getIntent().getStringExtra("dateTimeFrom");
                dateTo = getIntent().getStringExtra("dateTimeTo");
        }

        edtxtSearch = (AppCompatEditText) findViewById(R.id.edtxtSearch);
        btnSend = (Button) findViewById(R.id.btnSend);

        lstvwEmployee = (ListView) findViewById(R.id.lstvwEmployee);
        mAdapter = new EmployeeAdapter(this, Employee.getList());
        lstvwEmployee.setAdapter(mAdapter);

        edtxtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(edtxtSearch.getText().length() == 0){
                    mAdapter.clear();
                    mAdapter.notifyDataSetChanged();
                    lstvwEmployee.setVisibility(View.GONE);
                }

                if(edtxtSearch.getText().toString().equals("s")){
                    lstvwEmployee.setVisibility(View.VISIBLE);
                }

                if(edtxtSearch.getText().toString().equals("soon")){
                    lstvwEmployee.setVisibility(View.VISIBLE);
                    mAdapter = new EmployeeAdapter(InviteActivity.this, Employee.getListSoon());
                    lstvwEmployee.setAdapter(mAdapter);
                }
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(getResources().getString(R.string.server))
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                BackendService service = retrofit.create(BackendService.class);

                Room room = (Room)getIntent().getParcelableExtra("room");
                Meeting meeting = new Meeting();
                meeting.setRoomName(room.getName());
                meeting.setRoomId(room.getId());
                meeting.setContent(edtxtContent.getText().toString());
                meeting.setSubject(edtxtSubject.getText().toString());
                meeting.setEndTime(dateTo);
                meeting.setStartTime(dateFrom);

                //Call<Boolean> call = service.book();
                Call<Boolean> call = service.book2(meeting);


                call.enqueue(new Callback<Boolean>() {
                    @Override
                    public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                        Log.i("osy", response.toString());
                        AlertDialog.Builder builder = new AlertDialog.Builder(InviteActivity.this);
                        builder.setTitle("Success!");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                onBackPressed();
                            }
                        });
                        builder.setMessage("Meeting room has been booked");
                        builder.setIcon(R.drawable.ic_check_circle_green_24dp);
                        AlertDialog alert1 = builder.create();
                        alert1.show();
                    }

                    @Override
                    public void onFailure(Call<Boolean> call, Throwable t) {
                        Log.e("osy ", "", t);
                    }
                });

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //return super.onOptionsItemSelected(item);
        int id = item.getItemId();
        switch(id){
            default:
                onBackPressed();
                return super.onOptionsItemSelected(item);
        }
    }

}
