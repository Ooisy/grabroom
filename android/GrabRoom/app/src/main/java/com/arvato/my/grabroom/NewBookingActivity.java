package com.arvato.my.grabroom;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TimePicker;

import com.arvato.my.grabroom.data.model.Room;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class NewBookingActivity extends AppCompatActivity {

    private AppCompatEditText edtxtDateFrom;
    private AppCompatEditText edtxtDateTo;

    private ListView lstvwRoom;
    private RelativeLayout rlList;

    private CheckBox chkbxProjector;
    private CheckBox chkbxTV;
    private CheckBox chkbxWhiteboard;
    private CheckBox chkbxTeleCon;

    private DatePickerDialog.OnDateSetListener onDateFromSetListener;
    private DatePickerDialog.OnDateSetListener onDateToSetListener;

    private TimePickerDialog.OnTimeSetListener onTimeFromSetListener;
    private TimePickerDialog.OnTimeSetListener onTimeToSetListener;

    private SimpleDateFormat dateFormatter;

    private RoomAdapter mAdapter;

    private int yearFrom;
    private int monthFrom;
    private int dayFrom;

    private int yearTo;
    private int monthTo;
    private int dayTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_booking);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
////                        .setAction("Action", null).show();
//            }
//        });

        chkbxProjector = (CheckBox)findViewById(R.id.chkbxProjector);
        chkbxTV = (CheckBox)findViewById(R.id.chkbxTv);
        chkbxWhiteboard = (CheckBox)findViewById(R.id.chkbxWhiteboard);
        chkbxTeleCon = (CheckBox)findViewById(R.id.chkbxTeleconference);

        rlList = (RelativeLayout) findViewById(R.id.rlList);
        lstvwRoom = (ListView) findViewById(R.id.lstvwRoom);
        //mAdapter = new RoomAdapter(this, Room.getList());
        //lstvwRoom.setAdapter(mAdapter);

        edtxtDateFrom = (AppCompatEditText) findViewById(R.id.edtxtDateFrom);
        edtxtDateTo = (AppCompatEditText) findViewById(R.id.edtxtDateTo);

        edtxtDateFrom.setFocusable(false);
        edtxtDateFrom.setClickable(true);

        edtxtDateTo.setFocusable(false);
        edtxtDateTo.setClickable(true);

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());

        onDateFromSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                final Calendar calendar = Calendar.getInstance();
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);

                yearFrom = i;
                monthFrom = i1;
                dayFrom = i2;

                TimePickerDialog timePickerDialog = new TimePickerDialog(NewBookingActivity.this, onTimeFromSetListener, hour, minute, true);
                timePickerDialog.show();
            }
        };

        onTimeFromSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(yearFrom, monthFrom, dayFrom, i, i1);
                edtxtDateFrom.setText(dateFormatter.format(newDate.getTime()));
            }
        };

        onDateToSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                final Calendar calendar = Calendar.getInstance();
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE);

                yearTo = i;
                monthTo = i1;
                dayTo = i2;

                TimePickerDialog timePickerDialog = new TimePickerDialog(NewBookingActivity.this, onTimeToSetListener, hour, minute, true);
                timePickerDialog.show();
            }
        };

        onTimeToSetListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(yearTo, monthTo, dayTo, i, i1);
                edtxtDateTo.setText(dateFormatter.format(newDate.getTime()));
            }
        };

        edtxtDateFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                //Calendar minCalendar = Calendar.getInstance();
                //minCalendar.add(Calendar.MONTH, -3);

                DatePickerDialog datePickerDialog = new DatePickerDialog(NewBookingActivity.this, onDateFromSetListener, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(Calendar.getInstance().getTimeInMillis());
                //datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                datePickerDialog.show();
            }
        });

        edtxtDateTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);

                //Calendar minCalendar = Calendar.getInstance();
                //minCalendar.add(Calendar.MONTH, -3);

                Calendar minCalendar = Calendar.getInstance();
                if(yearFrom !=0 && monthFrom !=0 && dayFrom !=0){
                    minCalendar.set(yearFrom, monthFrom, dayFrom);
                }

                DatePickerDialog datePickerDialog = new DatePickerDialog(NewBookingActivity.this, onDateToSetListener, year, month, day);
                datePickerDialog.getDatePicker().setMinDate(minCalendar.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(minCalendar.getTimeInMillis());
                datePickerDialog.show();
            }
        });

        lstvwRoom.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(NewBookingActivity.this, InviteActivity.class);
                intent.putExtra("room", mAdapter.getItem(i));
                intent.putExtra("dateTimeFrom", edtxtDateFrom.getText().toString());
                intent.putExtra("dateTimeTo", edtxtDateTo.getText().toString());
                startActivityForResult(intent, 0);
            }
        });

        chkbxProjector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    rlList.setVisibility(View.VISIBLE);
                    mAdapter = new RoomAdapter(NewBookingActivity.this, Room.getListProjector());
                    lstvwRoom.setAdapter(mAdapter);
                }else{
                    mAdapter.clear();
                    mAdapter.notifyDataSetChanged();
                    rlList.setVisibility(View.GONE);
                }
            }
        });

        chkbxTV.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    rlList.setVisibility(View.VISIBLE);
                    mAdapter = new RoomAdapter(NewBookingActivity.this, Room.getListTV());
                    lstvwRoom.setAdapter(mAdapter);
                }else{
                    mAdapter.clear();
                    mAdapter.notifyDataSetChanged();
                    rlList.setVisibility(View.GONE);
                }
            }
        });

        chkbxWhiteboard.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    rlList.setVisibility(View.VISIBLE);
                    mAdapter = new RoomAdapter(NewBookingActivity.this, Room.getListWhiteboard());
                    lstvwRoom.setAdapter(mAdapter);
                }else{
                    mAdapter.clear();
                    mAdapter.notifyDataSetChanged();
                    rlList.setVisibility(View.GONE);
                }
            }
        });

        chkbxTeleCon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    rlList.setVisibility(View.VISIBLE);
                    mAdapter = new RoomAdapter(NewBookingActivity.this, Room.getListTeleCon());
                    lstvwRoom.setAdapter(mAdapter);
                }else{
                    mAdapter.clear();
                    mAdapter.notifyDataSetChanged();
                    rlList.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //return super.onOptionsItemSelected(item);
        int id = item.getItemId();
        switch(id){
            default:
                onBackPressed();
                return super.onOptionsItemSelected(item);
        }
    }
}
