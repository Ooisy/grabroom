package com.arvato.my.grabroom;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.arvato.my.grabroom.data.model.Room;

import java.util.List;

public class RoomAdapter extends ArrayAdapter<Room>{

    private final Context mContext;

    private List<Room> mList;
    private LayoutInflater mInflater;

    static class ViewHolder {
        protected TextView txtvwRoom;

        public ViewHolder(View rootView) {
            txtvwRoom = (TextView) rootView.findViewById(R.id.txtvwRoom);
        }
    }

    public RoomAdapter(Context context, List<Room> objects) {
        super(context, 0, objects);
        mContext = context;
        mList = objects;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Room getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //return super.getView(position, convertView, parent);
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_room, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.txtvwRoom.setText(mList.get(position).getName());

        return convertView;
    }
}
