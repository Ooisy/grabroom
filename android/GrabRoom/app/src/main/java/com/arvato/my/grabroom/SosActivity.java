package com.arvato.my.grabroom;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class SosActivity extends AppCompatActivity {
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sos);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dialog = ProgressDialog.show(SosActivity.this, "",
                "Checking for available room(s) now...", true);
        new LongOperation().execute("");

        Button button = (Button) findViewById(R.id.sosBookButton);
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SosActivity.this);
                builder.setTitle("Success!");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SosActivity.this.finish();
                    }
                });
                builder.setMessage("Langkawi has been booked for 30 mins from 12:30pm to 01:00pm");
                builder.setIcon(R.drawable.ic_check_circle_green_24dp);
                AlertDialog alert1 = builder.create();
                alert1.show();
            }
        });

    }

    private class LongOperation extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                Thread.interrupted();
            }
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            finishLoading();
        }

        @Override
        protected void onPreExecute() {}

        @Override
        protected void onProgressUpdate(Void... values) {}
    }

    private void finishLoading() {
        this.dialog.dismiss();
        findViewById(R.id.resultView).setVisibility(View.VISIBLE);
    }

}
