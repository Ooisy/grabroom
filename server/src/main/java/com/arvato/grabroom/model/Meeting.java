package com.arvato.grabroom.model;

import lombok.Data;

import java.util.Date;

@Data
public class Meeting {

    private String roomName;
    private String roomId;
    private String subject;
    private String startTime;
    private String endTime;
    private String content;

}
