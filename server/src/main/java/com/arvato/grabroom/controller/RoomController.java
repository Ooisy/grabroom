package com.arvato.grabroom.controller;

import com.arvato.grabroom.model.Meeting;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.activation.DataHandler;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

@RestController
@RequestMapping(value = "/")
@Api(value = "main endpoint for grabroom")
@Slf4j
public class RoomController {

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());


	@Value("${token}")
	private String token;

	@RequestMapping(value = "/book", method = RequestMethod.POST)
	public Boolean book() throws Exception {
		log.info("Room booking start");

		try(CloseableHttpClient client = HttpClients.createDefault();){
			HttpPost httpPost = new HttpPost("https://graph.microsoft.com/v1.0/me/events");

			String json = "{\n" +
					"    \"subject\": \"Team 1337 v2 Innovation Day room booking\",\n" +
					"    \"body\": {\n" +
					"        \"contentType\": \"HTML\",\n" +
					"        \"content\": \"This is a room booking test for innovation day\"\n" +
					"    },\n" +
					"    \"start\": {\n" +
					"        \"dateTime\": \"2018-09-07T20:00:00\",\n" +
					"        \"timeZone\": \"Asia/Singapore\"\n" +
					"    },\n" +
					"    \"end\": {\n" +
					"        \"dateTime\": \"2018-09-07T21:00:00\",\n" +
					"        \"timeZone\": \"Asia/Singapore\"\n" +
					"    },\n" +
					"    \"location\": {\n" +
					"        \"displayName\": \"ASY MY - Meeting Room - Penang (12-pax)\"\n" +
					"    },\n" +
					"    \"attendees\": [{\n" +
					"        \"emailAddress\": {\n" +
					"            \"address\": \"RMB10013606@internal.arvato.com\",\n" +
					"            \"name\": \"Penang\"\n" +
					"        },\n" +
					"        \"type\": \"required\"\n" +
					"    }]\n" +
					"}";
			StringEntity entity = new StringEntity(json);
			httpPost.setEntity(entity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setHeader("Authorization", "Bearer " + token);

			CloseableHttpResponse response = client.execute(httpPost);
			log.info("Response:" + response);
			log.info("Finished processing room booking request");
		}

		return Boolean.TRUE;
	}

	@RequestMapping(value = "/book2", method = RequestMethod.POST)
	public Boolean book2(@RequestBody Meeting meeting) throws Exception {
		log.info("Room booking start");
		Date startTime = dateFormatter.parse(meeting.getStartTime());
		Date endTime = dateFormatter.parse(meeting.getEndTime());

		try(CloseableHttpClient client = HttpClients.createDefault();){
			HttpPost httpPost = new HttpPost("https://graph.microsoft.com/v1.0/me/events");

			String json = "{\n" +
					"    \"subject\": \""+meeting.getSubject()+"\",\n" +
					"    \"body\": {\n" +
					"        \"contentType\": \"HTML\",\n" +
					"        \"content\": \"" + meeting.getContent() + "\"\n" +
					"    },\n" +
					"    \"start\": {\n" +
					"        \"dateTime\": \"" + sdf.format(startTime) + "\",\n" +
					"        \"timeZone\": \"Asia/Singapore\"\n" +
					"    },\n" +
					"    \"end\": {\n" +
					"        \"dateTime\": \"" + sdf.format(endTime) + "\",\n" +
					"        \"timeZone\": \"Asia/Singapore\"\n" +
					"    },\n" +
					"    \"location\": {\n" +
					"        \"displayName\": \""+meeting.getRoomName()+"\"\n" +
					"    },\n" +
					"    \"attendees\": [{\n" +
					"        \"emailAddress\": {\n" +
					"            \"address\": \""+ meeting.getRoomId() + "@internal.arvato.com\",\n" +
					"            \"name\": \"" + meeting.getRoomName() + "\"\n" +
					"        },\n" +
					"        \"type\": \"required\"\n" +
					"    }]\n" +
					"}";
			StringEntity entity = new StringEntity(json);
			httpPost.setEntity(entity);
			httpPost.setHeader("Accept", "application/json");
			httpPost.setHeader("Content-type", "application/json");
			httpPost.setHeader("Authorization", "Bearer " + token);

			CloseableHttpResponse response = client.execute(httpPost);
			log.info("Response:" + response);
			log.info("finished processing room booking request");
		}

		return Boolean.TRUE;
	}


}
